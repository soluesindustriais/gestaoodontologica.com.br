<section class="mb-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6"><img src="<?= $url ?>assets/img/dentistas.png" alt="Ilustração de dois dentistas"
                    class="img-fluid"></div>
            <div class="col-md-6 text-left p-5">
                <h2 class="mt-4 mb-3  text-uppercase h1 ">
                    O sonho do sorriso perfeito na palma das suas mãos
                </h2>
                <p>Cuidar da saúde bucal exige mais que as três escovações diárias. É preciso aliar a essa limpeza
                    obrigatória, visitas ao dentista e disciplina nos tratamentos em exercício.
                </p>
                <p>Foi pensando em todos os cuidados necessários para se ter o sorriso dos sonhos, que a Ideal Odonto
                    criou três planos odontológicos: a Odonto Kids, Odonto Quality e Odonto Orto. Cada um desses três
                    planos, possuem o objetivo de oferecer um atendimento específico e cobertura para cada necessidade.
                </p>

                <a class="btn btn-lg button-slider mt-2 " href="<?= $url ?>informacoes">Saiba mais</a>
            </div>
        </div>
    </div>
</section>