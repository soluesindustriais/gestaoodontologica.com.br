function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this,
            args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};



const target1 = document.querySelectorAll('[data-anime]');
const target2 = document.querySelectorAll('[data-anime2]');
const target3 = document.querySelectorAll('[data-anime3]');
const animationClass = 'animate';

function animeScroll() {
    const windowTop = window.pageYOffset + ((window.innerHeight * 3) / 4);
    target1.forEach(function (element) {
        if (windowTop > element.offsetTop) {
            element.classList.add(animationClass)
        } else {
            element.classList.remove(animationClass);
        }
    })
    setTimeout(function(){target3.forEach(function (element) {
        if (windowTop > element.offsetTop) {
            element.classList.add(animationClass)
        } else {
            element.classList.remove(animationClass);
        }
    })

    }, 1400)
    setTimeout(function(){target2.forEach(function (element) {
        if (windowTop > element.offsetTop   ) {
            element.classList.add(animationClass)
        } else {
            element.classList.remove(animationClass);
        }
    })

    }, 700)
}

animeScroll();
if (target1.length)  {
    window.addEventListener('scroll', debounce(function () {
        animeScroll();

    }, 50));
}


$('#carousel').on('slide.bs.carousel',function(){
	setTimeout(function(){
		 $('#carousel .carousel-inner .active').attr('data-anime="up"');
	}
), 300;
});


