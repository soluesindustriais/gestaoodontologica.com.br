var planos = {};


var settings = {
    'container'      : '#containerLista',
    'itemLista'      : '.containerLista',
    'tokenCliente'   : null,
    // Classes e template da listagem de planos;
    'tituloPlano'    : '.tituloPlano',
    'imagemPlano'    : '.imagemPlano',
    'btnContrata'    : '#btnContrata',
    'btnSaiba'       : '#btnSaiba',
    'descPlano'      : '.descPlano',
    'precoPlano'     : '.precoPlano',
    'contratarPlano' : '.contratarPlano',
    'linkPlano'      : '.linkPlano',
    'template'       : '<div class="col-md-4 col-12 containerLista" data-mh="planos">' +
    '                        <ul class="text-center p-0 m-0 price-one h-100" style="list-style-type: none; border: 2px solid #eee;-webkit-transition: 0.3s;transition: 0.3s;border-radius: 5px;">' +
    '                            <li class="pt-4 border">' +
    '                                <img class="imagemPlano" style="width: 90px;">' +
    '                                <h2 class="tituloPlano p-0 my-3" style="color: #333;    font-weight: 600;    font-size: 25px;"></h2>' +
    '                            </li>' +
    '                            <li class="border" style="min-height:170px"><div class="descPlano"></div></li>' +
    '                            <li class="border precoPlano" style="font-size: 48px;  font-weight: bold; color: #397cbc;"></li>' +
    '                            <li class="blue border"><a id="btnContrata" target="_blank" class="button contratarPlano">Contratar</a>' +
    '                            <a class="button btn-default linkPlano" id="btnSaiba">Saiba Mais</a></li>' +
    '                        </ul>' +
    '                    </div>',
    // Classes da página interna do plano;
    'contratarInternaPlano': '.linkContratarInterna',
    'precoInternaPlano': '.precoInternaPlano',
    'descInternaPlano': '.descInternaPlano'
};

// Buscar planos;
var planos = {};

function numberToReal(numero) {
    var numero = numero.toFixed(2).split('.');
    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}

function buscarPlanos(_token, _call){


    settings.token = _token;

    // url: 'http://bocaodonto.idealapps.com.br/obterPlanos',
    // url: 'http://app.bocaodonto.localhost/obterPlanos',

    $.ajax({
        type: "POST",
        dataType: "json",
        url: 'http://app.idealassist.com.br/obterPlanos',
        // url: 'http://app.bocaodonto.localhost/obterPlanos',
        // url: 'http://bocaodonto.idealapps.com.br/obterPlanos',
        data: {'token': _token},
        beforeSend: function(){
            $(settings.container).html('<p class="buscando-planos">Buscando planos...</p>');
        },
        success: function(data) {

            if(data.sucesso){

                planos = data.planos;

                if(typeof _call == 'function'){
                    _call();
                }

            }else{

                data.planos = {};
                $(settings.container).html('<p class="buscando-planos error">'+data.msg+'</p>');

            }

        },
        error: function(xhr, textStatus, errorThrown) {

        }
    });

}

function renderizarPlanos(utmid, utmsource){

var PlanoKids = "https://app.idealassist.com.br/formulario/index/c4ca4238a0b923820dcc509a6f75849b?user_id=939&origem_id=" + utmid + "&express=1&utm_source=" + utmsource;
var PlanoQuality = "https://app.idealassist.com.br/formulario/index/eccbc87e4b5ce2fe28308fd9f2a7baf3?user_id=939&origem_id=" + utmid + "&express=1&utm_source=" + utmsource;
var PlanoOrto = "https://app.idealassist.com.br/formulario/index/c81e728d9d4c2f636f067f89cc14862c?user_id=939&origem_id=" + utmid + "&express=1&utm_source=" + utmsource;


    $(settings.container).html('');

    $.each(planos, function(index, value){

		if (value.titulo.indexOf("Promoção") == -1){

	        // Adiciona o template;
	        $(settings.container).append(settings.template);

	        var ultimo = $(settings.itemLista).last();

	        // Preenche os dados do plano;
	        ultimo.find(settings.tituloPlano).html(value.titulo);
	        ultimo.find(settings.descPlano).html(value.conteudo);
	        ultimo.find(settings.precoPlano).html(value.preco);

	        //ultimo.find(settings.contratarPlano).attr('href', value.token);
	        if (value.titulo == "Odonto Kids"){
                ultimo.find(settings.contratarPlano).attr('href', PlanoKids);
                ultimo.find(settings.btnContrata).attr('onlcick', "fbq('track' , 'btn Planos - Contratar " + value.titulo + "'); ");
                ultimo.find(settings.btnSaiba).attr('onlcick', "fbq('track' , 'btn Planos - Saiba Mais " + value.titulo + "'); ");
            }    
	        if (value.titulo == "Odonto Quality"){
                ultimo.find(settings.contratarPlano).attr('href', PlanoQuality);
                ultimo.find(settings.btnContrata).attr('onlcick', "fbq('track' , 'btn Planos - Contratar " + value.titulo + "'); ");
                ultimo.find(settings.btnSaiba).attr('onlcick', "fbq('track' , 'btn Planos - Saiba Mais " + value.titulo + "'); ");
            } 
	        if (value.titulo == "Odonto Orto"){
                ultimo.find(settings.contratarPlano).attr('href', PlanoOrto);
                ultimo.find(settings.btnContrata).attr('onlcick', "fbq('track' , 'btn Planos - Contratar " + value.titulo + "'); ");
                ultimo.find(settings.btnSaiba).attr('onlcick', "fbq('track' , 'btn Planos - Saiba Mais " + value.titulo + "'); ");
            }
	        console.log(value.titulo)   
	            
	        if(value.imagem != null && value.imagem != ''){
	            // ultimo.find(settings.imagemPlano).attr('src', value.imagem);

	            /***
	                Ajuste apenas para subir o site,
	                subir as images corretas na api e retirar esse trecho do código
	                não esqueça de descomentar a linha acima
	            ***/
	            if(value.imagem == "http://app.idealassist.com.br/files/planos/plano-odonto-orto-1518705153.png"){
	                ultimo.find(settings.imagemPlano).attr('src', "http://app.idealassist.com.br/files/planos/orto.png");
	            } else if (value.imagem == "http://app.idealassist.com.br/files/planos/plano-odonto-quality-1518705199.png") {
	                ultimo.find(settings.imagemPlano).attr('src', "http://app.idealassist.com.br/files/planos/quality.png");
	            } else {
	                ultimo.find(settings.imagemPlano).attr('src', value.imagem);
	            }
	              /***
	                Fim do Ajuste
	            ***/

	        }else{
	            ultimo.find(settings.imagemPlano).css('display', 'none');
	        }

	        if(value.link_detalhes != null && value.link_detalhes != ''){
	            ultimo.find(settings.linkPlano).attr('href', 'http://'+ window.location.hostname +'/'+value.titulo.toLowerCase().replace(" ", "-"));
	        }else{
	            ultimo.find(settings.linkPlano).css('display', 'none');
	        }
		}	

    });

    // Ajusta as alturas;
    $('.heightLista').matchHeight({
        byRow: true
    });

    $('.containerLista .header').matchHeight();

    $('.containerLista .precoPlano').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

}

// Resgatar dados do plano pela URL;
function internaPlano(){

    $.each(planos, function(index, value){

        var link = window.location.href;
        link = link.replace('http://', '');
        link = link.replace('https://', '');

        if(value.link_detalhes == link){
            $(settings.contratarInternaPlano).attr('href', value.token);
            $(settings.precoInternaPlano).html(value.preco);
            $(settings.descInternaPlano).html(value.conteudo);

            $(settings.precoInternaPlano).priceFormat({
                prefix: 'R$ ',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });

        }

    });

}