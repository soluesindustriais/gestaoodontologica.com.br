    /*
    10/07/2019
    MKT IDEAL TRENDS
    Adaptado de: https://codepen.io/annalarson/pen/GesqK
    Lista de parametros do animate: https://www.w3schools.com/jquery/eff_animate.asp

    //DAVID
    ============
    Como Usar
        $(function () {
          lazycomp();
        });

        ou com parametros

        $(function () {
          lazycomp(classe,tempo,espacamento);
        });

    ============
    Configs
        lazyclass = "Classe do objeto a se esconder e mostrar (PADRÃO: .lazycomp)"
        lazytime = "Tempo da animação em MS (PADRÃO: 1200)" 
        lazyespacamento = "Espaço em PX a ser subtraido do minimo para mostrar a div (PADRÃO: 400)"
    ============

    */

function lazycomp(lazyclass=".lazycomp",lazytime=1200,lazyespacamento=400) {

    $(function () {
        $(lazyclass).fadeIn('slow');
        $(lazyclass).css('opacity','0');
    });


    $(document).ready(function () {
        /* Toda vez que a tela for scrollada ... */
        $(window).scroll(function () {
            /* checar a localização do elemento */
            $(lazyclass).each(function (i) {
                var bottom_of_object = ($(this).position().top + $(this).outerHeight()-lazyespacamento);
                var bottom_of_window = $(window).scrollTop() + $(window).height();
                /* IF objeto for completamente visivel */
                if (bottom_of_window > bottom_of_object) {
                    $(this).animate({
                        'opacity': '1',
                    }, lazytime);
                }
            });
        });
    });
}