<?php 

function tirarprepo($string){
    return preg_replace('/( a | ante | até | após | de | desde | em | entre | com | contra | para | por | perante | sem | sob | sobre | e | do | no | na | da )/i',' ',$string);
}

function array_search_multi($busca, $arrays){        
foreach($arrays as $array){        
        if(is_array($array)){        
            if(array_search_multi($busca,$array)) return true;        
        }else{            
            if($busca == $array) return true;            
        }      
    }   
    return false; 
}

function escolheplano($palavra){
$tipoplano = "";
$h1Exploded = explode(" ",tirarAcentos(tirarprepo($palavra)));

$orto =array('aparelho','orto', 'ortodontia');
$kids =array('criança','pediatria','pediatra','leite','pediatria', 'pediatrica','infantil','familiar');

for ($i=0; $i<sizeof($h1Exploded); $i++) {
    
    if(array_search_multi($h1Exploded[$i],$orto) != false){
        $tipoplano = 'odonto-orto';
        break;
    }else if(array_search_multi($h1Exploded[$i],$kids) != false){
        $tipoplano = 'odonto-kids';
        break;
    }else{
        $tipoplano = 'odonto-quality';
        
    }
}
    
    return $tipoplano;
}
?>
