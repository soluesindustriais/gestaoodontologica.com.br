<div class="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
	<div class="breadcrumb-item">
		<a href="<?= $url ?>" title="Home" itemprop="url">
			<span itemprop="title">Home</span>
		</a>
	</div>
	<?php foreach ($previousUrl as $key => $value) {
		echo 	"<div class=\"breadcrumb-item\" itemprop=\"child\" itemscope itemtype=\"https://data-vocabulary.org/Breadcrumb\">
							<a itemprop=\"url\" href=\"" . $url . titletourl($value['title']) . "\" title=\"" . $value['title'] . "\">
							<span itemprop=\"title\">" . $value['title'] . "</span>
				</a>
	</div>";
	}
	?>

	<div class="breadcrumb-item active" aria-current="page" title="<?= $h1 ?>" itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?= ucfirst($h1); ?></span></div>
</div>