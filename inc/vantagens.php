<section class="container my-5">
    <div class="row">
        <div class="col-12 mb-4">
            <h2 class="text-center text-uppercase h1">SABE QUAIS SERÃO SUAS VANTAGENS?</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6 card-circle-icon text-center py-2">
            <img src="assets/img/icon/profissionais.svg" class="mb-2" alt="Profissional" />
            <h2 class="text-uppercase m-0">PROFISSIONAL</h2>
            <p class="mt-1">Profissionais com reconhecimento no meio odontológico.
            </p>
        </div>
        <div class="col-lg-3 col-md-6 card-circle-icon text-center py-2">
            <img src="assets/img/icon/tratamento.svg" class="mb-2" alt="Tratamento" />
            <h2 class="text-uppercase m-0">TRATAMENTO ORTODÔNTICO</h2>
            <p class="mt-1">A Ideal Odonto cuida desde solicitação da sua documentação ao início do tratamento.
            </p>
        </div>
        <div class="col-lg-3 col-md-6 card-circle-icon text-center py-2">
            <img src="assets/img/icon/rede.svg" class="mb-2" alt="Rede credenciada" />
            <h2 class="text-uppercase m-0">REDE CREDENCIADA</h2>
            <p class="mt-1">A Ideal Odonto possui uma das maiores redes de parcerias do Brasil.
            </p>
        </div>
        <div class="col-lg-3 col-md-6 card-circle-icon text-center py-2">
            <img src="assets/img/icon/valor.svg" class="mb-2" alt="Preços" />
            <h2 class="text-uppercase m-0">PREÇO</h2>
            <p class="mt-1">Você obtém um plano odontológico por um preço justo.
            </p>
        </div>

    </div>


</section>