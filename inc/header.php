<div class="d-none" id="topo"></div>
<!-- <header class="sticky-top"> -->
<header>
  <nav id="menu" class="navbar navbar-expand-md navbar-light p-md-0 p-3">
    <div class="container-fluid">
      <a class="navbar-brand text-md-right col-9 col-md-2" href="<?=$url?>" title="<?=$nomeSite." - ".$slogan?>"><img src="<?=$url?>assets/img/logo-branco.png" alt="<?=$nomeSite." - ".$slogan?>" title="<?=$nomeSite." - ".$slogan?>"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbar1">
        <ul class="navbar-nav ml-auto">
          <?php include 'inc/menu.php' ?>
          <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="http://www.ans.gov.br/planos-de-saude-e-operadoras/informacoes-e-avaliacoes-de-operadoras/qualificacao-ans" target="_blank"><img src="<?=$url?>assets/img/ans.png" alt="ans-logo" class="pt-3 mt-1 logo-ans-topo"></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<?php if (($title != "Home") && ($pagInterna =="" || $pagInterna =="Planos")) { ?>
<section style="padding:40px 0px;background:#5298C1" class="<?=include('inc/escolhe-plano.php');?>">  
    <div class="container">
      <div class="row">
        <div class="col-12">         
          <h1 class="text-white text-center" style="text-shadow: 1px 1px 2px #000000;"><?=$h1?></h1>
            <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
            <?php include 'inc/breadcrumb.php' ?>
        </div>
      </div>
    </div> 
</section>
<?php } ?>