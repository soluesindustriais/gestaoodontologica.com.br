<div class="container mt-4">
    <div class="row">
        <div class="copyright col-12 py-4 col-md-7">
            <p><small>O conteúdo do texto desta página é de direito reservado. Sua reprodução, parcial ou total, mesmo
                    citando nossos links, é proibida sem a autorização do autor. Crime de violação de direito autoral –
                    artigo 184 do Código Penal – <a rel="noopener nofollow"
                        href="https://www.planalto.gov.br/Ccivil_03/Leis/L9610.htm" target="_blank"
                        title="Lei de direitos autorais">Lei 9610/98 - Lei de direitos autorais</a>.</small></p>
        </div>
    </div>
</div>