<aside class="mb-5">
	<?php
	if(isset($facebook) && !empty($facebook) ){
	echo "
	<div class=\"fb-page\" data-href=\"$facebook\" data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"false\" data-show-posts=\"false\"><div class=\"fb-xfbml-parse-ignore\"><blockquote cite=\"$facebook\"><a href=\"$facebook\">$nomeSite</a></blockquote></div></div>
	";
	echo "
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = \"//connect.facebook.net/pt_BR/all.js#xfbml=1\";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
	";
	}
	?>
	<a href="<?=$url?>informacoes" class="list-group-item aside-title" title="Informações"><h2 class="text-uppercase">Informações</h2></a>
	<?php
	function submenu($buffer)
	{
	return (str_replace("dropdown-item", "list-group-item", $buffer));
	}
	ob_start("submenu");
	include 'inc/sub-menu-informacoes.php';
	ob_end_flush();
	?>
	<h2 class="aside-contact">Entre em contato conosco</h2>
	<div class="col-md-12 aside-phone">
		<?php
		foreach ($fone as $key => $value) {
		echo "<a class=\"$value[1] d-block my-1\" href=\"tel:$ddd$value[0]\" title=\"Clique e Ligue\">$ddd <strong>$value[0]</strong></a>";
		}
		?>
		<div class="clearfix"></div>
	</div>
</aside>