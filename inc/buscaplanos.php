<?php
$urlarray = 'https://servertemporario.com.br/guardiao/jsons/planos.json';
$JSON = file_get_contents($urlarray);
$jsonObj = json_decode($JSON);
$planos = $jsonObj->planos;

foreach ($planos as $e) {
        $nome_plano = $e->nome;
        $urlimg_plano = $url . "assets/img/";
        // $urlimg_plano = $e->urlimg;
        $desc_plano = $e->desc;
        $preco_plano = $e->preco;

        echo '
            <div class="col-md-4 planos-card text-center">';
        if ($nome_plano == "Odonto Kids") {
            echo '
                <img class="planos-img" src="' . $urlimg_plano . 'kids.png" alt="' . $nome_plano . '">
                ';
        } elseif ($nome_plano == "Odonto Quality") {
            echo '
                <img class="planos-img" src="' . $urlimg_plano . 'quality.png" alt="' . $nome_plano . '">
                ';
        } else {
            echo '
                <img class="planos-img" src="' . $urlimg_plano . 'orto.png" alt="' . $nome_plano . '">
                ';
        };
        echo '
            
            <h2>' . $nome_plano . '</h2>
            <ul>
                ' . $desc_plano . '
            </ul>

            <p>
            <strong>Carência: liberado em 24 horas para emergências após o pagamento.</strong>
            </p>
            
            <h3>
                ' . $preco_plano . '
            </h3>
            <p>
             Cartão de Crédito ou Via Conta de Luz ** Verifique as regiões de atendimento 
            </p>
            
            ';
        if ($nome_plano == "Odonto Kids") {
            echo '
                    <a class="btn btn-lg button-slider px-0 text-white" data-toggle="modal" data-target="#form-modal" onclick="$("#pagina").val("Odonto KIDS");"> Adquirir Já</a>
                    <a class="btn button-slider " href="' . $url . '/odonto-kids" role="button">Saiba Mais</a>
                    ';
        } elseif ($nome_plano == "Odonto Quality") {
            echo '
                    <a class="btn btn-lg button-slider px-0 text-white" data-toggle="modal" data-target="#form-modal" onclick="$("#pagina").val("Odonto QUALITY");"> Adquirir Já</a>
                    <a class="btn button-slider" href="' . $url . '/odonto-quality" role="button">Saiba Mais</a>
                    ';
        } else {
            echo '
                    <a class="btn btn-lg button-slider px-0 text-white" data-toggle="modal" data-target="#form-modal" onclick="$("#pagina").val("Odonto ORTO");"> Adquirir Já</a>
                    <a class="btn button-slider" href="' . $url . '/odonto-orto" role="button">Saiba Mais</a>
                    ';
        };
        echo '
            
        </div>
        ';
    }


?>

<!-- Modal -->
<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="background-image: linear-gradient(120deg, #669df7 0%, #6eb4d4 100%);">

            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" style="color: #fff;opacity: 1;">×</button><br>
                <div class="formulario-lado mb-4">

                    <h3 class="formulario-h2 pb-2">DÚVIDAS DE QUAL PLANO ODONTOLÓGICO É IDEAL PARA VOCÊ? NÓS ENTRAMOS EM
                        CONTATO</h3>
                    <form id="form-odonto" method="post" name="form" action="javascript:chamarPlugin();">
                        <div class="form-group">
                            <label class="label-form"><i class="fa fa-user"></i> Nome</label>
                            <input name="nome" type="text" class="form-control" placeholder="Seu nome" required=""
                                id="nome">
                        </div>
                        <div class="form-group">
                            <label class="label-form"><i class="fa fa-envelope"></i> Endereço de email</label>
                            <input name="email" type="email" class="form-control" placeholder="Seu email" required=""
                                id="email">
                        </div>
                        <div class="form-group">
                            <label class="label-form"><i class="fa fa-phone"></i> Telefone</label>
                            <input name="telefone" type="tel" class="form-control telefone" placeholder="Seu telefone"
                                required="" id="telefone" maxlength="15">
                        </div>

                        <input type="hidden" name="pagina" value="" id="pagina">
                        <input type="hidden" name="site" value="Ideal Odonto" id="site">
                        <input type="submit" name="enviar" class="btn button-slider"
                            style="font-size:14px;padding:8px;text-align:center;width:100%;" value="SOCILITAR CONTATO">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>