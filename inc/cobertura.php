<section class="fundo-cobertura">
	<div class="container">
		<div class="row">
			<div class="col-12 mb-4">
				<h2 class="text-center text-uppercase h1 text-white">O que esse plano cobre?</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 py-2 cobertura text-white d-flex align-items-center">
				<img src="<?=$url?>assets/img/icon/check-01.png" alt="check">Cirurgia de Freio (Frenectomia)
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-01.png" alt="check">Extração de Dente de Leite
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-01.png" alt="check">Extração de Raiz
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-01.png" alt="check">Limpeza (Raspagem)
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-01.png" alt="check">Tratamento de Canal
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-01.png" alt="check">Aplicação de Flúor
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-01.png" alt="check">Restauração/Obturação
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-01.png" alt="check">Sanar Sangramento e Inchaço
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-01.png" alt="check">Procedimentos Protéticos
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center ">
				<img src="<?=$url?>assets/img/icon/check-0<?= ($h1 != "Odonto Kids") ? 1 : 2; ?>.png" alt="check">Extração de siso
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-0<?= ($h1 == "Odonto Orto") ? 1 : 2; ?>.png" alt="check">Manutenção de Aparelho Fixo
			</div>
			<div class="col-md-3 py-2 cobertura text-white d-flex flex-row align-items-center">
				<img src="<?=$url?>assets/img/icon/check-0<?= ($h1 == "Odonto Orto") ? 1 : 2; ?>.png" alt="check">Documentação de Aparelho
			</div>
		</div>
	</div>
</section>