<li class="nav-item">
	<a class="nav-link menu-text" href="<?=$url?>" title="Página inicial">Home <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
	<a class="nav-link menu-text" href="<?=$url?>quem-somos" title="Quem Somos">Quem Somos</a>
</li>
<li class="nav-item dropdown">
	<a class="nav-link menu-text" href="<?=$url?>planos" title="Planos">Planos</a>
	 <div class="dropdown-menu" style="min-width:200px!important;left: -70px;">
		<a class="dropdown-item" href="<?=$url?>odonto-kids" title="Odonto Kids">Odonto Kids</a>
		<a class="dropdown-item" href="<?=$url?>odonto-quality" title="Odonto Quality">Odonto Quality</a>
		<a class="dropdown-item" href="<?=$url?>odonto-orto" title="Odonto Orto">Odonto Orto</a>
	</div>
</li>
<li class="nav-item dropdown">
    <a class="nav-link menu-text" rel="nofollow" href="https://ssl.datanext.com.br/sistema-datasys/mod-app/1032/busca.html?search_type=av" target="_blank" title="Rede Credenciada">Rede Credenciada</a>
	 <div class="dropdown-menu" style="min-width:240px!important;left: -50px;">
		<a class="dropdown-item" rel="nofollow" href="https://ssl.datanext.com.br/sistema-datasys/mod-app/1032/busca.html?search_type=av"  target="_blank" title="Consulte Aqui">Consulte Aqui</a>
		<a class="dropdown-item" href="<?=$url?>contato" title="Torne-se um Credenciado">Torne-se um Credenciado</a>
	</div>
</li>
<li class="nav-item dropdown">
	<a class="nav-link menu-text" href="<?=$url?>informacoes" title="Informações">Informações</a>
	 <ul class="dropdown-menu">
		<?php include 'inc/sub-menu-informacoes.php'; ?>	
	</ul>
</li>
<li class="nav-item">
	<a class="nav-link menu-text" href="<?=$url?>duvidas" title="Dúvidas?">Dúvidas?</a>
</li>
<li class="nav-item">
	<a class="nav-link menu-text" href="<?=$url?>contato" title="Contato">Contato</a>
</li>
<li class="nav-item">
	<a class="nav-link menu-text btn-area" rel="nofollow" href="https://ssl.datanext.com.br/sistema-datasys/mod-aut/" title="Área do Prestador"  target="_blank" ><i class="fas fa-bars"></i> Área do Prestador</a>
</li>
<li class="nav-item">
	<a class="nav-link menu-text btn-area-azul" rel="nofollow" href="https://ssl.datanext.com.br/sistema-datasys/mod-app/1032/"  target="_blank"  title="Área do Cliente" ><i class="fas fa-bars"></i> Área do Cliente</a>
</li>

