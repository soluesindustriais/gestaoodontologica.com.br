<?php
$h1 = "Quem Somos";
$title = $h1;
$desc = "A Ideal Odonto nasceu por meio da experiência de um grupo consolidado há 20 anos no mercado, composto por 12 empresas de diferentes segmentos";
include('inc/head.php');
?>

<body>

    <?php include 'inc/header.php' ?>
    <main>
        <div class="container my-5">
            <div class="row">
                <div class="col-md-12 mb-5 text-center">
                    <div class="row align-items-center">

                        <div class="col-md-6">
                            <p>
                                A Ideal Odonto é uma empresa seguradora de três planos odontológicos: Odonto Kids,
                                Odonto Quality e Odonto Orto. O objetivo dos planos é atender qualquer pessoa,
                                independente da idade e necessidade que tiverem.
                            </p>
                            <p>
                                A empresa tem o objetivo de tornar a maior aquisitora de parceiros do país. Além disso,
                                tem a grande missão de oferecer independência aos seus clientes, pois quer possibilitar
                                que eles escolham a clínica e quais profissionais de sua preferência e próximo da região
                                onde residem.
                            </p>
                            <p>
                                A Ideal Odonto quer presentear seus clientes, com seus melhores sorrisos.


                            </p>
                        </div>
                        <div class="col-md-6">
                            <img src="assets/img/aboutus1.png" alt="Ilustração de dois dentistas" class="img-fluid">
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <img src="assets/img/aboutus2.png" alt="Ilustração de dois dentistas" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                            <p class="text-center">A missão da Ideal Odonto é se tornar a maior seguradora de planos
                                odontológicos contratada do país, além disso, a empresa deseja oferecer uma ampla
                                cobertura para tratamentos odontológicos aos seus clientes.


                            </p>

                            <p class="text-center">A principal missão da Ideal Odonto é tornar mais fácil o acesso aos
                                planos odontológicos, pois quer está presente na vida de todas as famílias brasileiras.


                            </p>


                        </div>
                    </div>

                </div>


            </div>

        </div>
    </main>
    <?php include 'inc/footer.php' ?>
</body>

</html>