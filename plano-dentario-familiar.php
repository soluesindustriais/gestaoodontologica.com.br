<?php
include('inc/vetKey.php');
$h1 = "plano dentário familiar";
$title = $h1;
$desc = "Gestante e bebê: benefícios do plano dentário familiar Dentre os principais benefícios em se contratar um benefícios do plano dentário familiar, está";
$key = "plano,dentário,familiar";
$legendaImagem = "Foto ilustrativa de plano dentário familiar";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                    <h2><!--StartFragment-->Gestante e bebê: benefícios do plano dentário familiar</h2><div>Dentre
 os principais benefícios em se contratar um benefícios do plano 
dentário familiar, está a disponibilidade de serviços de assistência a 
saúde bucal para gestantes e bebes. Considerando que, os lábios fazem 
parte de uma região que se forma entre a quarta e a sétima semanas de 
gestação e, conforme o feto se desenvolve durante a gestação, o tecido 
corporal e células de cada lateral da cabeça crescem em direção ao 
centro do rosto, para  então se juntarem e formarem a aparência da face,
 o lábio leporino é classificado como uma disfunção que, ocorre quando o
 tecido que forma os lábios e o palato não se juntam por completo.</div><div> </div><div> O 
resultado é uma abertura na região superior da boca, na qual, pode ser 
pequena e atingir somente os lábios ou, quando maior, atingir até o céu 
da boca e o nariz. Pesquisadores e 
especialidades ainda não identificam, exatamente a causa do lábio 
leporino. Porém, sabe-se que, essa disfunção está ligada a uma 
combinação de fatores que, envolvem genética e o ambiente da mãe durante
 a gravidez. Algumas das atitudes na gravidez podem favorecer o lábio 
leporino como, o hábito de fumar, consumir bebidas alcoólicas, utilizar 
de certos medicamentos para tratar epilepsia ou enxaqueca e, a má 
higiene bucal.</div><div> </div><div><h2>Diagnósticos com plano dentário familiar</h2></div><div>
<!--StartFragment-->Neste cenário, o plano 
dentário familiar pode aumentar as chances de prevenção contra possíveis
 complicações na formação do feto e, ainda, no parto. Além de, melhorar a
 qualidade de vida da gestante.<!--EndFragment-->

O
 lábio leporino pode ser identificado ainda na gestação, por intermédio 
de um ultrassom de rotina e, os principais tratamentos são: </div><div> </div><ul><li>Realização
 de cirurgias para a correção do problema;</li><li> Realização de consultas 
periódicas ao dentista; </li><li>Orientação em relação a cuidados especiais na 
amamentação;</li><li> Orientação em relação a cuidados de higiene bucal. </li></ul><div> </div><div><h2>Pequenos pacientes e plano dentário familiar</h2></div><div> Um
 plano dentário familiar, ainda, pode auxiliar para complicações menos 
severas mas, que podem acometer os pequenos pacientes, a exemplo da 
fluorose dentária. Identificada como manchas, em geral esbranquiçadas, 
que surgem nos dentes pelo excesso de flúor, na maioria dos casos de 
forma simétrica. Geralmente, acomete crianças de 0 a 12 anos em 
localidades onde a água é fluoretada ou, possui nível de fluoreto maior 
que 4 mg/L. </div><div> </div><div>Esse excesso de manchas pode causar a
 fluorose dentária, constituída por alterações na tonalidade dos 
elementos dentários que, varia desde amarelo-claro para castanho, devido
 à corrosão do esmalte dos dentes. Esta condição pode ser, facilmente, 
evitada através de um plano dentário familiar e, além da utilização dos 
serviços, associar boas práticas de cuidados para com a higiene bucal da
 criança. </div><div> </div>Ademais, especialistas afirmam 
que, crianças menores de seis anos são mais propensas a ingestão 
prolongada de fluoreto, uma vez que por vezes são incapazes de 
administro, de maneira adequada, cremes dentais e, controlar o consumo 
do flúor em alimentos. Sendo assim, contar com plano dentário familiar, 
pode garantir a segurança, não somente de beneficiários adultos mas, 
também de bebês e crianças ainda na fase de desenvolvimento da dentição.<!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>