<?php
$h1 = "Informações";
$title = $h1;
$desc = "Tratamentos de canal, prótese, obturações, limpezas, extrações de siso e muito mais. Contamos com profissionais especialistas em todos os tratamentos odontológicos, prontos para atender qualquer emergência";
include('inc/head.php'); ?>


<body>

    <?php include 'inc/header.php' ?>
   

    <main>
        <div class="container mb-5">
            <div class="row">
                <div class="col-md-12">
                    <?php include 'inc/breadcrumb.php' ?>
                </div>

                <div class="col-md-12">


                    <div class="card-deck" itemscope itemtype="https://schema.org/Products">



                        <div class="row w-100 m-0" id='example'>
                            <?php
                           include_once('inc/vetKey.php');
					   foreach ($vetKey as $key => $vetor) {   ?>
                           
                           
					  <div class='col-md-3 p-0 mb-4'>
									<a href='<?= $vetor['url']; ?>' title='<?= $vetor['key']; ?>'>
                            <div class='card'>
                                <img class='card-img-top' src='<?= $url; ?>assets/img/img-mpi/350x350/<?= $vetor['url']; ?>-1.jpg' alt='<?= $vetor['key']; ?>' title='<?= $vetor['key']; ?>'>

                                <div class='card-footer p-1 d-flex justify-content-center text-center align-items-center' style='height:50px'>
                                    <h2 class='h4 text-uppercase m-0'>
                                        <?= $vetor['key']; ?>
                                    </h2>
                                </div>
                            </div>
                        </a>
					</div>
					
					<?php }?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>
    <?php include 'inc/footer.php' ?>
</body>

</html>
