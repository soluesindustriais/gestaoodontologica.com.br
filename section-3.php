<section class="container mb-5">

    <div class="row">
        <div class="col-md-4 text-center px-4">
            <img src="assets/img/icon/missao.png" alt="Missão" class="mb-2" style="width:120px" />
            <h2 class="text-uppercase h1 m-0">Missão</h2>
            <p class="mt-1">A missão da Ideal Odonto é ajudar a melhorar a autoestima de seus clientes através da
                odontologia. Ou seja, disponibilizando planos odontológicos.
            </p>
            <p class="mt-1">
                Contudo, para a empresa cumprir essa missão, é preciso também ampliar a carteira de parceiros para
                garantir que os melhores procedimentos sejam aplicados por quem entende do assunto. Portanto, se tornar
                a responsável pelos sorrisos de seus clientes, é oferecer os melhores planos odontológicos e possuir
                parcerias com os melhores dentistas.
            </p>
        </div>
        <div class="col-md-4 text-center px-4">
            <img src="assets/img/icon/visao.png" alt="Visão" class="mb-2" style="width:120px" />

            <h2 class="text-uppercase h1 m-0">Visão</h2>
            <p class="mt-1">A Ideal Odonto não quer apenas vender planos odontológicos , mas por meio deles, oferecer os
                melhores planos do país.
            </p>
            <p class="mt-1">
                A empresa Ideal Odonto tem o foco na ampliação de parcerias e em se tornar excelente em seu atendimento,
                qualidade no trabalho, pelas recomendações e por ser a melhor operadora odontológica do Brasil.

            </p>
        </div>
        <div class="col-md-4  text-center px-4">
            <img src="assets/img/icon/valores.png" alt="Valores" class="mb-2" style="width:120px" />
            <h2 class="text-uppercase h1 m-0">Valores</h2>
            <p class="mt-1">Trabalhamos com atendimentos humanuzados e equipes especializadas em transformar sorrisos e
                garantir a autoestima dos clientes. Trabalhamos com todas as especialidades, os planos da Ideal Odonto
                foram criados para cuidar de cada cliente de maneira personalizada, atravéz de uma relação de respeito e
                transparência.</p>
        </div>


    </div>
</section>