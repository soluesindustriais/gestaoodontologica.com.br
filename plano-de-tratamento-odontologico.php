<?php
include('inc/vetKey.php');
$h1 = "plano de tratamento odontológico";
$title = $h1;
$desc = "Saúde em dia com plano de tratamento odontológico Se enganam aqueles que pensam que, um plano de tratamento odontológico trata apenas da saúde da boca";
$key = "plano,de,tratamento,odontológico";
$legendaImagem = "Foto ilustrativa de plano de tratamento odontológico";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                    <h2><!--StartFragment-->Saúde em dia com plano de tratamento odontológico</h2><div>Se
 enganam aqueles que pensam que, um plano de tratamento odontológico 
trata apenas da saúde da boca e dos dentes. A cada consulta, o dentista 
da rede credenciada é capaz de avaliar o quadro clínico da saúde bucal, 
assim como o restante do corpo, caso o profissional suspeite da 
existência de algum desequilíbrio como alterações nos índices de glicose
 ou baixa imunidade, será orientado ao paciente a necessidade de buscar 
por um especialista. </div><div> </div><div>A saúde bucal anda conjunto com a saúde do corpo e,
 ao se consultar com o dentista, o paciente tem sua saúde como um todo 
avaliada. Como é habitual que, as manifestações bucais demonstrem 
sintomas característicos na região bucal, optar por um plano de 
tratamento odontológico acaba sendo uma medida preventiva. Buscando pela
 ajuda odontológica desde cedo, a probabilidade de um tratamento ser 
mais eficiente e, de se alcançar a cura se elevam. </div><div> </div><div>O
 número de enfermidades que um dentista consegue identificar pela 
cavidade oral é enorme e, entram na lista, doenças como a aids, bulimia,
 vários tipos de câncer cirrose hepática, diabetes, papilomavírus humano
 ou, como é conhecido popularmente: HPV, osteoporose, refluxo estomacal,
 clamídia, sífilis e, até mesmo a gonorreia. Ter um plano de tratamento 
odontológico aumenta a possibilidade de sanar certar complicações e, 
evitar gerar preocupações desnecessárias, através da prevenção e do 
tratamento feito a tempo. </div><div> </div><div><h2>Doenças evitadas pelo plano de tratamento odontológico</h2></div><div>Um
 distúrbio odontológico que, tende a atingir milhares de pessoas em todo
 o país é o Bruxismo. Diferente do que se pode acreditar, a dor nos 
dentes ou de cabeça e mandíbula, pode ter consequências graves quando 
não tratadas da forma adequada. Considerado pela Odontologia e Medicina 
como um hábito parafuncional, considerando que,  o indivíduo não percebe
 que está com a alteração comportamental, por estar inconsciente, o 
bruxismo provoca o ranger rítmico dos elementos dentários e, pode ser 
evitado e/ou tratado a partir da contratação dos serviços de um plano de
 tratamento odontológico. Sua causa está associada, de maneira direta, 
ao índice de estresse que o paciente vem sofrendo, seja pressão: </div><div> </div><ul><li>No trabalho;</li><li> Familiar; </li><li>Social; </li><li>Pessoal. </li></ul><div>Em
 muitos dos casos, os indivíduos não tem conhecimento de que possuem o 
hábito inconsciente de ranger os dentes e, só descobrem em uma avaliação
 ou exame odontológicos de rotina. Toda disfunção caracterizada como 
bruxismo é, identificada como um apertamento dentário. Entretanto têm 
pessoas que possuem todos os sintomas e, não sofrem com o apertamento 
dentário. </div><div> </div><div><h2>Orientações do plano de tratamento odontológico</h2></div><div>Para
 quem sofre com algum dos sintomas citados ou, até mesmo que não sinta 
os incômodos mas, está em falta com as consultas regulares ao dentista, 
vale a pena a contratação de um plano de tratamento odontológico e, 
assim realizar uma avaliação. Se identificado que é necessário tratar o 
bruxismo, o profissional irá orientar sobre quais atitudes deverão ser 
tomadas para, evitar o desgaste dos dentes. Além do mais, contar com um 
plano de tratamento odontológico se torna um investimento lucrativo e 
notável a curto prazo.

</div>

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>