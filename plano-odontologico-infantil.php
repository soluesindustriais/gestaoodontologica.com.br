<?php
include('inc/vetKey.php');
$h1 = "plano odontológico infantil";
$title = $h1;
$desc = "plano odontológico com ortodontia O tempo total de um tratamento ortodôntico é muito variável e depende, entre outras coisas, dos problemas dos";
$key = "plano,odontológico,infantil";
$legendaImagem = "Foto ilustrativa de plano odontológico infantil";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->

                <h2>Dente de leite: cuidados do plano odontológico infantil</h2>
                <p>Ainda
                    que, a reabsorção dentária fisiológica, dos dentes decíduos (ou como
                    são, popularmente chamados, os dentes de leite), esteja programada de
                    maneira genética e, definida como de caráter temporário. Para este
                    processo, acontece a interação de causas anatômicas, bioquímicas,
                    mecânicas e genéticas. </p>
                <p>A erupção habitual, tanto no que diz respeito ao
                    tempo, quanto a posição de cada elemento dentário, é um dos processos
                    naturais e decisivos no desenvolvimento das arcadas dentárias. Enquanto,
                    a rizólise do dente decíduo, acometida pela erupção do dente sucessor
                    permanente, é um fenômeno considerado fisiológico e dinâmico. Ou seja, a
                    demora da remoção de um dente de leite, quando já apontado a peça
                    definitiva, pode acarretar uma série de complicações que, vão além da
                    estética desarmônica do sorriso. </p>
                <p>A
                    retenção prolongada dos molares decíduos exige atenção por parte do
                    auxílio de um profissional e, com um plano odontológico infantil, é
                    possível impedir que certas consequências negativas se perdurem pela
                    fase adultas. Visto que, a erupção dos sucessores, quando ainda faltam
                    espaços para seu desenvolvimento, pode causar danos à oclusão do pequeno
                    paciente. Especial atenção deve ser dada para a substituição dos dentes
                    caninos e molares decíduos, pelos seus sucessores correspondentes.
                    Através do plano odontológico infantil, a erupção deste grupo de dentes
                    obedece a uma sequência que favoreça a oclusão e alinhamento da
                    dentição. </p>

                <h2>Saiba como utilizar o plano odontológico infantil</h2>
                <p>O
                    diagnóstico e a intervenção precoce em casos de retenção prolongada dos
                    molares decíduos são de fundamental importância para minimizar ou até
                    mesmo evitar danos à oclusão, o que evidencia as vantagens em se ter um
                    plano odontológico infantil. A retenção prolongada dos molares decíduos é
                    causada por questões locais, ambientais e genéticas, podendo promover o
                    desenvolvimento de más oclusões, à medida que altera a sequência
                    natural de erupção das peças dentárias permanentes.</p>

                <p>O tratamento
                    adequado exige a exodontia dos dentes retidos, seguida da manutenção da
                    área, além de controle periódico até o nascimento dos pré-molares. Além
                    desses procedimentos, é possível que um plano odontológico infantil,
                    cubra a realização de exames como:</p>

                <ul>
                    <li>Tomografia convencional; </li>
                    <li>Tomografia computadorizada; </li>
                    <li>Radiografia 3D; </li>
                    <li>Radiografia Panorâmica. </li>
                </ul>

                <p>A
                    radiografia panorâmica é uma das principais recomendações dos
                    profissionais da área, visto que, proporciona uma análise global,
                    fornecendo informações morfológicas e, ainda, a cronologia de nascimento
                    dos dentes decíduos e permanentes, em uma única radiografia. </p>

                <h2>Exames cobertos pelo plano odontológico infantil</h2>
                <p>Assim
                    como outros exames cobertos pelo plano odontológico infantil, a
                    radiografia panorâmica é uma medida suplementar ao diagnóstico clínico,
                    para avaliar as variações das estruturas dento-maxilares e afecções
                    patológicas da gengiva e osso. Contudo, esse método pode apresentar
                    limitações, em relação à observação de locais com mais detalhes e, em
                    casos como este, há a necessidade de complementação por meio de outras
                    técnicas radiográficas.
                </p>
                <p>Na maior parte das vezes, esses exames são
                    cobertos de maneira total pelo plano odontológico infantil mas, a
                    depender das especificações de cada empresa responsável pela prestação
                    dos serviços, é possível que existam alterações quanto a cobertura. Por
                    esse motivo, é essencial que o interessado na contratação desse tipo de
                    assistência, conheça todas as disponibilidades da operadora.
                </p>


                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>