<?php
include('inc/vetKey.php');
$h1 = "convenio dentista";
$title = $h1;
$desc = "Convenio dentista e os principais benefícios O ideal para ter dentes bonitos, o que muitas pessoas almejam e priorizam, realizando o cuidado com a";
$key = "convenio,dentista";
$legendaImagem = "Foto ilustrativa de convenio dentista";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>Convenio dentista e os principais benefícios</h2>
                <p>O ideal para ter dentes bonitos, o que muitas pessoas almejam e priorizam, realizando o cuidado com a
                    aparência, é buscar auxílio no convenio dentista, já que o dentista é o profissional mais indicado
                    para manter a saúde bucal de qualidade ao mesmo tempo em que fornece um sorriso esteticamente
                    agradável. </p>
                <p>As pessoas podem desenvolver problemas ou doenças bucais ao longo da vida, as quais irão necessitar
                    de auxílio profissional com um convenio dentista verdadeiramente qualificado e de confiança. Se não
                    forem tratadas da forma ideal, essas doenças podem se desenvolver, provocando o enfraquecimento ou a
                    perda parcial ou total dos dentes. Com isso, torna-se necessário dar uma atenção para a região
                    bucal, não somente por motivos estéticos como também por questão de saúde.</p>
                <p>Visitar consultórios odontológicos é fundamental para todas as pessoas, desde as mais preocupadas com
                    o sorriso até as que não se importam, isso porque uma saúde bucal de qualidade é o que permite às
                    pessoas a realização de atividades fáceis e complexas, desde mastigar até trabalhar. Por conta
                    disso, em busca de maior comodidade e facilidade, as pessoas têm procurado cada vez mais a adesão a
                    algum convenio dentista.</p>
                <h2>O convenio dentista e suas modalidades</h2>
                <p>De acordo com uma pesquisa, acredita-se que mais de 25 milhões de pessoas no país já contam com algum
                    tipo de plano odontológico. Segundo a Agência Nacional de Saúde (ANS), tal número tende a saltar
                    para 50 milhões em apenas cinco anos. Por isso, conforme o tempo passa, as pessoas, cada vez mais,
                    vão buscar tratamento em clínicas que atendam o convenio dentista que contrataram. </p>
                <p>Nos dias de hoje, existem muitas vantagens para as pessoas que buscam contratar um bom convenio
                    dentista, como os preços mais baixos que os consultórios particulares e a ampla cobertura de
                    procedimentos. Por conta disso, é necessário pesquisar muito a respeito e escolher o convênio que
                    forneça o melhor custo-benefício. </p>
                <p>Em tal sentido, existem diversas modalidades do convenio dentista, voltados para diferentes
                    necessidades, bolsos e gostos. Dessa maneira, podem ser optadas as seguintes modalidades:</p>
                <ol>
                    <li>
                        <p>Convenio dentista familiar: nessa modalidade, o beneficiado pode incluir filhos, marido,
                            esposa, pai e mãe, lembrando que o plano dentário preço final varia de acordo com a idade,
                            doenças pré-existentes e estilo de vida de cada um dos dependentes. Por isso, recomenda-se
                            colocar na ponta do lápis e analisar se essa modalidade apresenta um custo acessível
                            financeiramente;</p>
                    </li>
                    <li>
                        <p>Convenio dentista individual: esse convênio permite ao indivíduo realizar diferentes tipos de
                            procedimentos que estão cobertos no convênio. O plano odontológico individual é indicado
                            para quem não deseja incluir outras pessoas e não tem um convênio da empresa. Por outro
                            lado, ele costuma ser um dos tipos de convênios mais caros;</p>
                    </li>
                    <li>
                        <p>Convenio dentista coletivo por adesão: nesse tipo, os beneficiados geralmente são sindicatos
                            e associações de determinadas profissões. Logo, a modalidade do plano analisa as
                            características do grupo como um todo e não das pessoas;</p>
                    </li>
                    <li>
                        <p>Convenio dentista empresarial: já nesse caso, o beneficiado não possui qualquer tipo de
                            relação com boletos ou burocracias do plano dentário preço, porque a própria empresa ou
                            alguma empresa terceirizada que se envolve com a responsabilidade relacionada a ele. A
                            contratação é de responsabilidade da empresa pública ou privada, tornando-se bastante
                            econômico e podendo ser descontado diretamente na folha de pagamento do empregado. Há também
                            a possibilidade de ser contratado por empresas de pequeno, médio e grande porte.</p>
                    </li>
                </ol>
                <h2>Conheça os tratamentos que estão disponíveis no convenio dentista</h2>
                <p><a href="<?= $url ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i++; ?>.jpg" data-fancybox="group1"
                        class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img
                            src="<?= $url; ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i - 1; ?>.jpg"
                            class="galeria centro" alt="<?= $h1; ?>"></a>A cobertura dos procedimentos odontológicos
                    pode ser bem variadas de acordo com a modalidade optada pelo beneficiado. Mas, existe uma lista de
                    procedimentos que todos os convênios precisam seguir. Em tal contexto, estão os procedimentos de
                    nível baixo, que contam com a consulta inicial para avaliação, aplicação de flúor, remoção de
                    tártaro, profilaxia (limpeza), restauração e tratamento de cáries.</p>
                <p>Nos procedimentos de nível médio encontram-se os curativos e suturas, periodontia (tratamento e
                    cirurgia da gengiva), colagem de fragmentos. Já em relação aos procedimentos de nível elevado, o
                    beneficiado pode fazer a exodontia (extração dos dentes), endodontia (tratamento de canal), biópsia
                    e cirurgias de pequeno porte. Por fim , a Agência Nacional de Saúde Suplementar (ANS) também
                    determina que qualquer plano dentário preço faça a cobertura de diferentes tipos de radiografia,
                    como a radiografia oclusal, a radiografia periapical e a radiografia bite-wing.</p>
                <p>De tal forma, os problemas e doenças bucais podem afetar outras regiões do corpo e proporcionar dores
                    de cabeça, dente e mandíbula, além de proporcionar dificuldades em funções relacionadas à
                    mastigação, fala e respiração. Em tal situação, vale a pena contratar o convenio dentista e, assim,
                    passar a cuidar melhor da saúde bucal.</p>
                <p>Por causa disso, o paciente pode realizar a limpeza, também chamada de profilaxia. Esse procedimento
                    elimina placas bacterianas e placas bacterianas calcificadas, conhecidas também como tártaro. Com
                    isso, a limpeza ajuda na manutenção da saúde bucal e evita que o paciente tenha o desenvolvimento de
                    doenças bucais. Na busca por maior eficiência, o recomendado é que a limpeza seja feita, no mínimo,
                    duas vezes por ano.</p>
                <p>O beneficiado ainda pode realizar a endodontia, famosamente conhecida como tratamento de canal.
                    Quando a cárie não é cuidada logo na fase inicial e chega na dentina, acontece a necessidade de
                    realizar a endodontia. Tal procedimento tem como objetivo principal abrir um buraco no dente,
                    inserir limas, que são pequenas agulhas, e retirar a polpa contaminada. Por conta disso, o
                    profissional é capaz de realizar a descontaminação e limpeza da região.</p>
                <p>Além disso, exrste também o tratamento de gengiva. A periodontia, conhecida também de tratamento de
                    gengiva, serve para desinflamar a região gengival. De tal forma, o dentista costuma realizar a
                    limpeza nos dentes, eliminando tártaros e placas bacterianas. Em situações em que o paciente
                    apresenta a gengiva bastante inflamada, o profissional pode também passar antibióticos e
                    anti-inflamatório.</p>
                <p>Outro tratamento que encontra-se disponível no convenio dentista é o tratamento de mau hálito. Para
                    acabar com o mau hálito, o procedimento mais comum no consultório odontológico é fazer um
                    diagnóstico do paciente, baseando-se nos hábitos diários, sintomas e histórico de doenças. Também
                    podem ser pedidos exames clínicos para descobrir se há doenças na cavidade oral ou pouco fluxo de
                    saliva. Logo depois da conclusão do diagnóstico, o dentista decide qual vai ser o melhor tratamento
                    para o caso, já que não existe um padrão para a realização do tratamento de mau hálito. </p>
                <h2>Extração do dente do siso pelo convenio dentista</h2>
                <p>Popularmente conhecido como dente do juízo ou terceiro molar, o dente do siso costuma surgir entre a
                    fase da adolescência e a vida adulta, por volta dos 17 aos 25 anos. Quando acontece a erupção, o
                    dente pode passar a inflamar com frequência, fazendo aparecer a pericoronarite e, por isso, muitos
                    pacientes buscam os consultórios odontológicos para fazer a extração.</p>
                <p>De maneira geral, as pessoas contam com dois sisos inferiores e dois superiores, mas nem sempre todos
                    eles aparecem. Em tal caso, são chamados de sisos inclusos, que estão presentes na arcada dentária,
                    porém dentro da gengiva e não ocorre a erupção. Ainda assim, eles podem ocasionar dor e, em algumas
                    situações, torna-se preciso removê-los se eles estiverem fazendo pressão sobre os dentes adjacentes.
                </p>
                <p>Há muitas razões para arrancar o siso pelo convenio dentista, mas somente um cirurgião qualificado e
                    de confiança é capaz de analisar e indicar a cirurgia. Em tal contexto, os principais fatores que
                    levam o paciente até a mesa de cirurgia para retirar o dente do siso é a pressão, pois o
                    empurramento nos dentes adjacentes ao siso é capaz de causar o desalinhamento da arcada, criando a
                    necessidade de iniciar um tratamento ortodôntico para corrigi-la.</p>
                <p>Além disso, existe a causa da conexão. Os sisos podem estar conectados com a raiz dos segundos
                    molares, ocasionando dor e, em certos casos, a necessidade de realizar a endodontia (tratamento de
                    canal). Outro motivo para retirar o siso é a pericoronarite. A frequente inflamação do dente do siso
                    é conhecida como pericoronarite. Em tal caso, o siso não é higienizado da maneira que precisava ser,
                    já que a sua posição dificulta a escovação certa, então ele passa a inflamar frequentemente por
                    acumular restos de comida e, consequentemente, promover o acúmulo e proliferação de bactérias.</p>
                <p>Além disso, há também a alteração. Afinal, o siso é capaz de prejudicar funções fundamentais do
                    organismo, tais como a fala, a respiração e a mastigação. Em outros casos, o siso também pode
                    provocar dor. Em certas pessoas, o siso pode ocasionar dores na face, chegando a atingir áreas como
                    mandíbula e ouvido.</p>
                <h2>Convenio dentista e seus benefícios</h2>
                <p>Atualmente, ainda que tenha um número grande de dentistas no Brasil, não são todas as pessoas que
                    pensam ser necessário marcar uma consulta no tempo recomendado, que é de, ao menos, duas vezes no
                    ano. Isso ocorre devido ao preço das consultas e tratamentos em clínicas particulares pode ser
                    bastante elevado, então o convenio dentista aparece como uma opção com atraente custo-benefício para
                    o cliente. </p>
                <p>O cuidado fundamental e correto com a saúde bucal necessita se tornar um hábito na vida de todas as
                    pessoas, já que afeta o funcionamento do corpo todo. Uma simples dor de dente pode ficar ainda mais
                    grave em pouco tempo e impedir que o sujeito consiga levar a vida normalmente com as atividades
                    diárias. Por conta disso, com o convenio dentista, torna-se possível diagnosticar, prevenir e tratar
                    problemas ou doenças bucais de diferentes gravidades. Nesse contexto, há diversos benefícios ao
                    contratá-lo, como: </p>
                <ul>
                    <li>
                        <p>Tempo de carência menor em relação aos planos de saúde em geral;</p>
                    </li>
                    <li>
                        <p>Custo-benefício mais atraente em relação às consultas em consultórios particulares;</p>
                    </li>
                    <li>
                        <p>Tratamentos preventivos e corretivos;</p>
                    </li>
                    <li>
                        <p>Equipe profissional especializada no cuidado e tratamento dentário infantil; </p>
                    </li>
                    <li>
                        <p>Cobertura em todo o Brasil.</p>
                    </li>
                </ul>
                <h2>Conheça os procedimentos estéticos disponíveis no convenio dentista</h2>
                <p>Além dos procedimentos com o objetivo de tratar as doenças e problemas bucais, como cáries, doenças
                    periodontais, entre outros, há alguns tipos de convenio dentista que possibilitam fazer
                    procedimentos estéticos. Em tal sentido, existe a ortodontia, que é o sub ramo da odontologia
                    responsável por fazer a movimentação dos dentes e ossos maxilares do paciente, com o objetivo de
                    torná-los mais harmônicos e adequadamente alinhados. </p>
                <p>Essa especialidade é voltada para pessoas que mostram dentes tortos ou desalinhados, provocando
                    prejuízos tanto estéticos quanto funcionais. Em tal contexto, para corrigir os defeitos
                    ortodônticos, podem ser utilizados os mais variados tipos de aparelho.</p>
                <p>Fora isso, ter um sorriso branco é uma meta que pode proporcionar confiança e aumento da autoestima
                    das pessoas, além de, inegavelmente, possibilitar dentes mais bonitos e apresentáveis. Com esse
                    objetivo em mente, os dentistas realizam o clareamento dental para fornecerem tais vantagens aos
                    pacientes que desejam recuperar o aspecto branco dos dentes.</p>
                <p>Por fim, em certos casos, há determinados tipos de convenio dentista que fornecem o implante
                    dentário. A perda dos dentes é algo que prejudica não apenas a estética do rosto como também as
                    funções da mastigação, fala e respiração, que são essenciais para o organismo humano. Por conta
                    disso, os pacientes que sofreram uma grande quantidade de perda têm o interesse em saber de algum
                    convenio dentista que cobre implante, já que o procedimento pode ser feito nos consultórios
                    odontológicos e possibilita a recuperação de um sorriso completo, mas costuma ter um preço elevado.
                </p>
                <h2>Ortodontia coberta pelo convenio dentista</h2>
                <p><a href="<?= $url ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i++; ?>.jpg" data-fancybox="group1"
                        class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img
                            src="<?= $url; ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i - 1; ?>.jpg"
                            class="galeria centro" alt="<?= $h1; ?>"></a>Além de prejudicar a estética do sorriso, os
                    dentes tortos ou as arcadas dentárias desalinhadas podem ocasionar o mau funcionamento de atividades
                    fundamentais para o organismo, como a mastigação, a respiração e a fala. De maneira geral, uma das
                    principais razões para começar um tratamento ortodôntico é o apinhamento, no qual o indivíduo
                    apresenta uma curta arcada dentária com diversos dentes.</p>
                <p>Outro problema é a mordida cruzada (os dentes superiores não se alinham corretamente com os
                    inferiores na oclusão) e mordida aberta (em que os dentes superiores não encostam nos dentes
                    inferiores na oclusão). Dessa forma, torna-se imprescindível utilizar algum tipo aparelho.Nesse
                    contexto, revela-se a necessidade de aderir a um aparelho ortodôntico por meio de um convenio
                    dentista, que pode ser algumas das seguintes opções:</p>
                <ol>
                    <li>
                        <p>Aparelho fixo estético de porcelana: promovendo uma resistência elevada e cor próxima ao
                            dente natural, o aparelho de porcelana se destaca entre as opções de aparelho estético;</p>
                    </li>
                    <li>
                        <p>Aparelho fixo lingual: conhecido também como aparelho interno, o aparelho lingual apresenta a
                            estrutura do aparelho fixo metálico, mas é colocado atrás dos dentes, em contato com a
                            língua e não com os lábios, por isso se torna praticamente imperceptível. Dessa forma, o
                            paciente também pode escolher diferentes cores para os bráquetes, como o aparelho de dente
                            preto. Por outro lado, essa opção não é muito acessível financeiramente, já que o preço pode
                            custar até quatro vezes mais que o aparelho fixo comum;</p>
                    </li>
                    <li>
                        <p>Aparelho invisível: também conhecido como aparelho transparente, o aparelho invisível é
                            removível e fabricado com acetato transparente, sendo considerado uma das opções mais sutis
                            do mercado odontológico, porém é indicado somente para os casos ortodônticos mais simples e
                            de curta duração;</p>
                    </li>
                    <li>
                        <p>Aparelho fixo metálico: é a modalidade mais comum e barata entre todos os tipos de aparelho,
                            possibilitando que o paciente escolha diferentes cores para as borrachinhas dos bráquetes.
                            Além disso, esse aparelho promove as movimentações necessárias dos dentes e da arcada
                            dentária, cuja eficiência existe tanto para casos ortodônticos simples quanto graves;</p>
                    </li>
                    <li>
                        <p>Aparelho fixo estético de safira: possibilitando um resultado estético superior em relação às
                            outras opções de aparelho estético, o aparelho de safira é o mais discreto, porém tem um
                            preço maior que os demais.</p>
                    </li>
                </ol>
                <h2>Convenio dentista realiza implante dentário</h2>
                <p>É muito importante destacar que os implantes oferecem diversos benefícios ao paciente, como a
                    não-rejeição do corpo ao implante, um tempo curto de cirurgia (por volta de 30 minutos para cada
                    implante) e a chance de duração indeterminada do implante caso o paciente siga o cuidado adequado
                    com a saúde bucal. Nesse contexto, as opções de implante dentário disponíveis são:</p>
                <ol>
                    <li>
                        <p>Zircônia: sendo apresentado como um potencial substituto ao titânio, a zircônia fornece
                            vantagens, mas ainda não é muito utilizada nos consultórios odontológicos. Aliás, os estudos
                            sobre material para esse tipo de material ainda não são conclusivos. De qualquer forma, não
                            tem como negar que a cor branca da zircônia é muito semelhante ao dente e apresenta uma
                            ótima resistência;</p>
                    </li>
                    <li>
                        <p>Implante protocolo: o formato do implante protocolo é recomendado para quem perdeu muitos
                            dentes e faz uso da dentadura diariamente. Esse tipo de implante possibilita a inserção de
                            uma prótese fixa, permitindo que o paciente tenha maior conforto e mobilidade na região
                            bucal, sem precisar do uso da dentadura novamente;</p>
                    </li>
                    <li>
                        <p>Implante curto: por ter dimensões pequenas e perfuração mínima na estrutura óssea, esse tipo
                            de implante é indicado para as pessoas que não têm a quantidade óssea suficiente na boca e
                            não querem fazer o enxerto ósseo antes do implante, já que ele é um procedimento caro;</p>
                    </li>
                </ol>
                <ol start="4">
                    <li>
                        <p>Titânio: quanto ao material, o cilindro de titânio ainda é o mais amplamente utilizado.
                            Afinal, ele possui grandes vantagens como a altamente resistência, leveza e compatibilidade
                            com o organismo humano. </p>
                    </li>
                </ol>
                <p>De acordo com o Instituto Brasileiro de Geografia e Estatística (IBGE), por volta de 50% dos adultos
                    brasileiros têm 20 ou menos dentes funcionais. Já na população idosa, tal número é ainda mais grave,
                    pois 70% deles não têm a quantidade ideal de dentes funcionais. Para solucionar esse problema,
                    colocar a prótese dentária depois do implante se revela como uma interessante alternativa na área da
                    odontologia. </p>
                <p>Por razão disso, é necessário buscar as opções de prótese no convenio dentista. Em atl sentido,
                    existe a prótese dental removível. Popularmente chamada de dentadura, essa é a prótese mais indicada
                    para quem perdeu grande quantidade de dentes ou todos eles. Há também a prótese dental parcialmente
                    fixa. Popularmente conhecida como ponte ou coroa, essa prótese é mais recomendada para quem perdeu
                    um ou poucos dentes, enquanto os outros ao redor continuam intactos. Dessa maneira, vale a pena
                    contratar um convenio dentista.</p>
                <h2>Convenio dentista realiza clareamento</h2>
                <p><a href="<?= $url ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i++; ?>.jpg" data-fancybox="group1"
                        class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img
                            src="<?= $url; ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i - 1; ?>.jpg"
                            class="galeria centro" alt="<?= $h1; ?>"></a>O clareamento dental mostra uma alta taxa de
                    adesão nos consultórios odontológicos em geral, já que proporciona um resultado muito satisfatório
                    para os pacientes. Entretanto, não são todas as pessoas podem fazer esse procedimento: lactantes,
                    grávidas, pacientes com irritação na gengiva, sensibilidade nos dentes e restaurações severas são
                    alguns dos grupos de risco. </p>
                <p>Antes de realizar o procedimento, é comum que as pessoas busquem na internet todos os tipos de
                    informações sobre esse assunto, pesquisem sobre o preço do clareamento dental e leiam os relatos de
                    pacientes que já fizeram o clareamento dental. Entretanto, realizar isso pode promover prejuízo, já
                    que nem tudo que está escrito na internet é a realidade.</p>
                <p>Por razão disso, torna-se fundamental conversar com o dentista credenciado pelo convenio dentista
                    abertamente, para ter conhecimento sobre o procedimento e as opções relacionadas a ele nos mínimos
                    detalhes. Para tirar todas as dúvidas de quem está interessado no assunto, há algumas informações
                    importantes. </p>
                <p>O clareamento à laser possibilita um resultado ainda mais rápido que o caseiro, já que, para alguns
                    pacientes, uma única sessão é suficiente. E muitas dúvidas existem a respeito do procedimento, mas
                    sanando uma delas, não, o clareamento não deixa os dentes mais sensíveis. Lembrando, é claro, que
                    alimentos ou bebidas com forte pigmentação, como café e refrigerantes de cola, precisam ser evitadas
                    depois que o procedimento tiver sido realizado. Aliás, o efeito do clareamento nos dentes pode durar
                    por vários anos caso o paciente siga de forma adequada os cuidados indicados pelo dentista.</p>
                <p>Em tal contexto, há vários tipos de clareamento. O clareamento à laser, por exemplo, é realizado no
                    consultório odontológico com luz pulsada, em que o resultado esperado pelo paciente pode acontecer
                    em apenas três sessões.</p>
                <p>Da mesma forma como o clareamento à laser, o clareamento caseiro necessita de um dentista para
                    realizar o acompanhamento do paciente. A diferença, em tal caso, é que ele vai utilizar uma moldeira
                    feita pelo profissional com uma substância clareadora. Nesse sentido, o gel clareador vai depender
                    da cor dos dentes do paciente. Após a fabricação da moldeira, ela deve ser utilizada em casa sob as
                    recomendações de tempo e frequência do dentista. No geral, o tempo de uso é de, aproximadamente, 15
                    dias. </p>
                <p>Já no caso do clareamento de farmácia, o paciente acha kits de clareamento em farmácia. Os kits desse
                    tipo de procedimento são autoaplicáveis, os quais contêm normalmente gel clareador, moldeira
                    pré-fabricada ou tiras branqueadoras. O paciente pode conseguir o resultado espeardo com esse
                    clareamento, mas o recomendado é sempre que a pessoa não faça o procedimento sem antes consultar um
                    dentista.</p>
                <p>Com isso, é possível ter um sorriso mais branco sem gastar um rio de dinheiro com o preço do
                    clareamento dental. Em tal caso, é importante lembrar também que é essencial realizar o procedimento
                    com um profissional de confiança, para que assim seja possível ter o resultado ideal. Mas com um
                    convenio dentista, não é preciso se preocupar com o valor do procedimento.</p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>