<?php
$h1 = "Contato";
$title = $h1;
$desc = "Entre em contato e envie sua mensagem pelo formulário e logo entraremos em contato. Qualquer dúvida estamos a disposição pelo email ou telefone";
include('inc/head.php');
?>

<body>

    <?php include 'inc/header.php' ?>
    <main>

        <section class="container my-5">
            <div class="row">
                <div class="col-md-6 col-12 py-3">
                    <?php $link = "https://servertemporario.com.br/guardiao/idealodonto/formulariocontato.xml";

                    $xml = simplexml_load_file($link);
                    foreach ($xml as $item) {  ?>

                        <h2 class="text-center text-uppercase h3">
                            <?= $item->titulo; ?>
                        </h2>
                        <p class="h4 text-center">
                            <?= $item->subtitulo; ?>
                        </p>
                        <?php $valor1 = rand(1, 9);
                        $valor2 = rand(1, 9);
                        $valorsoma = $valor1 + $valor2;
                        echo str_replace("#valorsoma#", $valorsoma, str_replace("#valor2#", $valor2, str_replace("#valor1#", $valor1, $item->campos)));
                    } ?>

                </div>
                <div class="col-md-6 col-12 py-3">
                    <h2 class="text-center text-uppercase h3">
                        <?= $nomeSite ?>
                    </h2>
                    <p class="h4 text-center mb-5">Avenida das Nações Unidas, 18801 - Conjunto 1309 - Santo Amaro - São
                        Paulo-SP - CEP: 04795-100</p>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7309.271575180734!2d-46.720448!3d-23.653211!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce51226419a41f%3A0x5c57ef41f1ffebfc!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+18801+-+V%C3%A1rzea+de+Baixo%2C+S%C3%A3o+Paulo+-+SP%2C+04795-090!5e0!3m2!1spt-BR!2sbr!4v1556567400905!5m2!1spt-BR!2sbr" height="395" class="w-100" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </section>
    </main>

    <?php include 'inc/footer.php' ?>
</body>

</html>