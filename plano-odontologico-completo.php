<?php
include('inc/vetKey.php');
$h1 = "plano odontológico completo";
$title = $h1;
$desc = "Guia Definitivo: plano odontológico completo Uma vantagem pouco falada entre as considerações de se contratar um plano odontológico completo, é a";
$key = "plano,odontológico,completo";
$legendaImagem = "Foto ilustrativa de plano odontológico completo";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>


                <h2>Guia Definitivo: plano odontológico completo</h2>
                <p>Uma
                    vantagem pouco falada entre as considerações de se contratar um plano
                    odontológico completo, é a possibilidade de tratamento para a disfunção
                    de DTM/DOF. O termo DTM corresponde a: disfunção Temporomandibular e, a
                    sigla DOF quer dizer: dor Orofacial. Ambas se referem às mudanças
                    funcionais que, um inpíduo pode apresentar em sua musculatura da face,
                    parte do corpo responsável por funcionalidades essenciais como a
                    mastigação e, também a articulação temporomandibular, conhecida como
                    ATM, que se localiza entre a mandíbula e o crânio, antes da orelha. </p>
                <p>Dentre
                    os sinais e sintomas dessa condição que, podem ser percebidos através
                    do respaldo de um plano odontológico completo, as cefaleias, dificuldade
                    de mastigar, incômodos na musculatura do rosto, perda de qualidade do
                    sono e até, dor de ouvido. Certos pacientes relatam que, já acordam pela
                    manhã apresentando indícios de desconfortos na face ou na cabeça e, não
                    associam ao fato de terem pressionado os dentes durante a noite. </p>
                <p>Esta
                    disfunção não possui um fator em específico, entretanto determinados
                    hábitos predispõem seu aparecimento, como herança genética, estresse,
                    depressão, postura corporal, roer unhas em excesso e, pressionar de
                    maneira inconsciente os dentes. São três as variações de DTM,
                    classificadas como: muscular, articular e mista. </p>
                <h2>Importância do plano odontológico completo</h2>
                <p>Normalmente,
                    a disfunção temporomandibular que acarreta dores orofaciais, atingem
                    os adultos. Por consequência do estado emocional de extremo estresse,
                    como dito anteriormente e, hábitos prejudiciais como: </p>
                <ul>
                    <li>Dormir de bruços; </li>
                    <li>Mastigar só de um lado da boca; </li>
                    <li>Roer unha; </li>
                    <li>Mascar muito chicletes; </li>
                    <li>Uso de próteses mal adaptadas </li>
                </ul>
                <p>Dentre
                    outras razões, um plano odontológico completo pode assegurar a
                    realização de procedimentos específicos que, solucionem questões como
                    estas. É comum confundir os sintomas dessa
                    condição com outros tipos de doenças, acreditando que não se trata de
                    algo tão sério. Mesmo sendo casos mais selecionados, certas crianças e
                    adolescentes podem apresentar sintomas de DTM/DOF. </p>
                <p>São pacientes que,
                    desenvolvem manifestações transitórias, as quais aparecem,
                    principalmente, na fase de crescimento e desenvolvimento, apresentando
                    consequências negativas sobre a qualidade de vida - considerando que,
                    uma criança que chupa dedo ou chupeta, tem chance de desenvolver a
                    DTM/DOF. O sono também pode sofrer oscilações que, tendem a afetar de,
                    maneira direta, a capacidade de raciocínio e, consequentemente o
                    desempenho escolar.</p>
                <h2>Formas de tratamento do plano odontológico completo</h2>

                <p>Primeiramente,
                    é importante ressaltar que, nem todos os tipos de plano odontológico
                    completo disponibilizam a assistência para tratamentos de DTM e, está é
                    uma condição crônica, ou seja: não tem cura. O que não é motivo de
                    desespero, afinal é possível controlar e, até mesmo prevenir o seu desenvolvimento.
                    Dentre as técnicas que, podem ser utilizadas pelo plano
                    odontológico completo, estão as placas transparentes, para controlar o
                    aperto dos dentes durante o sono, certos medicamentos e cirurgia, já em
                    último caso.</p>
                <p>
                    Ademais, é preciso lembrar
                    que o caso você não tenha um plano odontológico completo e, se
                    identificou com algum sintoma ou, situação aqui citados, busque por um
                    profissional! Somente ele poderá avaliar sua saúde bucal como um todo e
                    identificar a necessidade de realizar o tratamento.
                </p>



            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>